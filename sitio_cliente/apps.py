from django.apps import AppConfig


class SitioClienteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sitio_cliente'
