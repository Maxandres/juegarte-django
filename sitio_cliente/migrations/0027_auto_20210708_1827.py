# Generated by Django 3.2.4 on 2021-07-08 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitio_cliente', '0026_alter_pedido_fecha'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewarte',
            name='user',
            field=models.CharField(default='maxan', max_length=150),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reviewjuego',
            name='user',
            field=models.CharField(default='maxan', max_length=150),
            preserve_default=False,
        ),
    ]
