# Generated by Django 3.2.4 on 2021-07-07 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitio_cliente', '0009_reviewarte'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviewjuego',
            name='puntaje',
        ),
        migrations.AddField(
            model_name='reviewjuego',
            name='recomienda',
            field=models.CharField(choices=[('1', 'No lo Recomienda'), ('2', 'Lo Recomienda')], default=2, max_length=1),
        ),
    ]
