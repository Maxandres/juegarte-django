# Generated by Django 3.2.4 on 2021-07-08 04:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitio_cliente', '0019_alter_juegocarrito_juego'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='itemcarrito',
            name='arte',
        ),
        migrations.RemoveField(
            model_name='itemcarrito',
            name='juego',
        ),
        migrations.AddField(
            model_name='itemcarrito',
            name='precio',
            field=models.IntegerField(default=21),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='itemcarrito',
            name='producto',
            field=models.CharField(default=21, max_length=250),
            preserve_default=False,
        ),
    ]
