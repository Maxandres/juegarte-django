
from django.db.models.query import InstanceCheckMeta
from django.shortcuts import get_object_or_404, redirect, render
from sitio_admin.models import Juego, Arte
from .forms import FrmRegistro, formreviewarte, formreviewjuego,frmLog
from .filters import filtroJuego,filtroArte
from .models import  ItemPedido, Pedido, cart, itemCarrito,  reviewArte, reviewjuego
from django.contrib.auth import authenticate,login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required,permission_required
# Create your views here.

def customLogin(request):
    form=frmLog()
    contexto={
        "form":form
    }
    if request.method=="POST":
        form=frmLog(data=request.POST)
        print(form)
        if form.is_valid():
            user=authenticate(username=form.cleaned_data["username"],password=form.cleaned_data["password"])
            tipo=User.objects.filter(username=form.cleaned_data["username"])
            login(request,user)
            print("***********SI HIZO LOGIN*********************")
            if tipo==1:
                
                return redirect(to="sitio_admin/home.html")
            else:
                return redirect(to="home")
        else:
            print("***********No válido*********************")

    return render(request,"registration/frmLog.html",contexto)

def home(request):
    juegos=Juego.objects.all().filter(estado=1).order_by("idjuego")
    arte=Arte.objects.all().filter(estado=1).order_by("idarte")
    contexto={
        "juegos":juegos,
        "arte":arte
    }
    return render(request,"sitio_cliente/home.html",contexto)


def catalogojuegos(request):
    juegos=Juego.objects.all().filter(estado=1).order_by("idjuego")
    filtro=filtroJuego(request.GET,queryset=juegos)
    juegos=filtro.qs
    contexto={
        "juegos":juegos, "filtro":filtro,
       
    }

    return render(request,"sitio_cliente/catalogojuegos.html",contexto)


def detallejuego(request,id):
    juego=get_object_or_404(Juego,idjuego=id)
    formulario=formreviewjuego(request.POST or None)
    reviews=reviewjuego.objects.filter(juego_id=id)
    reviewuser=reviewjuego.objects.all()
    poseereview=0
    for item in reviewuser:
        if item.user== request.user.username and item.juego==juego:
            poseereview=1

    contexto={
        "juego":juego,
        "formulario":formulario,
        "reviews":reviews,
        "poseereview":poseereview
    }   
    if request.method=="POST":
        if formulario.is_valid():
            rev=reviewjuego()
            datos=formulario.cleaned_data
            rev.juego=juego
            rev.user=request.user.username
            rev.review=datos.get("review")
            rev.recomienda=datos.get("recomienda")
            rev.save()
            return redirect(to="catalogojuegos")
    return render(request,"sitio_cliente/detallejuego.html",contexto)


def catalogoarte(request):
    arte= Arte.objects.all().filter(estado=1).order_by("idarte")
    filtro=filtroArte(request.GET,queryset=arte)
    arte=filtro.qs
    contexto={
        "arte":arte,
        "filtro":filtro
    }

    return render(request,"sitio_cliente/catalogoarte.html",contexto)

def detallearte(request,id):
    arte=get_object_or_404(Arte,idarte=id)
    formulario=formreviewarte(request.POST or None)
    reviews=reviewArte.objects.all().filter(arte_id=id)
    reviewuser=reviewArte.objects.all()
    poseereview=0
    for item in reviewuser:
        if item.user == request.user.username and item.arte==arte:
            poseereview=1

    contexto={
        "arte":arte,
        "formulario":formulario,
        "reviews":reviews,
        "poseereview":poseereview
    }
    if request.method=="POST":
        if formulario.is_valid():
            rev=reviewArte()
            datos=formulario.cleaned_data
            rev.arte=arte
            rev.user=request.user.username
            rev.review=datos.get("review")
            rev.recomienda=datos.get("recomienda")
            rev.save()
            return redirect(to="catalogoarte")
    return render(request,"sitio_cliente/detallearte.html",contexto)    



def registro(request):
    form=FrmRegistro()

    contexto={
        "form":form
    }

    if request.method=="POST":
        form=FrmRegistro(data=request.POST)
        
        if form.is_valid():
            form.save()
            return redirect(to="login")
    return render(request,'registration/registro.html',contexto)        



@login_required
def carrito(request):
    carritos=cart.objects.all()
    tienecarrito=0
    for carrito in carritos:
        if carrito.user==request.user.username:
            tienecarrito=1
    
    if tienecarrito==1:
        carro=cart.objects.get(user=request.user.username)
        items=itemCarrito.objects.all().filter(user=request.user.username)
    else:
        carro=cart()
        carro.user=request.user.username
        carro.total=0
        carro.save()
        items=itemCarrito.objects.all().filter(user=request.user.username)
    total=0

    contexto={
       'items':items ,
       'carro':carro
       
    }
    return render(request,'sitio_cliente/carrito.html',contexto)

@login_required
def agregarjuegocarrito(request,id):
    carritos=cart.objects.all()
    item=Juego.objects.get(idjuego=id)
    juegoscarrito=itemCarrito.objects.all()
    existeitem=0
    tienecarrito=0
    for juego in juegoscarrito:
        if juego.user==request.user.username and juego.producto==item.nombrejuego:
            existeitem=1
    for carrito in carritos:
        if carrito.user==request.user.username:
            tienecarrito=1


    if tienecarrito==0:
        nuevo=cart()
        nuevo.user=request.user.username
        nuevo.total=0
        nuevo.save()

    if existeitem==0:
        nuevoitem=itemCarrito()
        nuevoitem.user=request.user.username
        nuevoitem.producto=item.nombrejuego
        nuevoitem.precio=item.preciojuego
        nuevoitem.cantidad=1
        nuevoitem.subtotal=item.preciojuego
        nuevoitem.save()
        carrito=cart.objects.get(user=request.user.username)
        carrito.total+=nuevoitem.subtotal
        carrito.save()
    elif existeitem==1:
        updateitem=itemCarrito.objects.get(producto=item.nombrejuego,user=request.user.username)
        updateitem.precio=item.preciojuego
        updateitem.cantidad+=1
        updateitem.subtotal=updateitem.precio*updateitem.cantidad
        updateitem.save()
        carrito=cart.objects.get(user=request.user.username)
        carrito.total+=updateitem.precio
        carrito.save()
    
   
    return redirect(to="catalogojuegos")

@login_required
def agregarartecarrito(request,id):
    carritos=cart.objects.all()
    item=Arte.objects.get(idarte=id)
    artecarrito=itemCarrito.objects.all()
    existeitem=0
    tienecarrito=0
    for arte in artecarrito:
        if arte.user==request.user.username and arte.producto==item.nombrearte:
            existeitem=1
    for carrito in carritos:
        if carrito.user==request.user.username:
            tienecarrito=1


    if tienecarrito==0:
        nuevo=cart()
        nuevo.user=request.user.username
        nuevo.total=0
        nuevo.save()

    if existeitem==0:
        nuevoitem=itemCarrito()
        nuevoitem.user=request.user.username
        nuevoitem.producto=item.nombrearte
        nuevoitem.precio=item.precioarte
        nuevoitem.cantidad=1
        nuevoitem.subtotal=item.precioarte
        nuevoitem.save()
        carrito=cart.objects.get(user=request.user.username)
        carrito.total+=nuevoitem.subtotal
        carrito.save()
    elif existeitem==1:
        updateitem=itemCarrito.objects.get(producto=item.nombrearte,user=request.user.username)
        updateitem.precio=item.precioarte
        updateitem.cantidad+=1
        updateitem.subtotal=updateitem.precio*updateitem.cantidad
        updateitem.save()
        carrito=cart.objects.get(user=request.user.username)
        carrito.total+=updateitem.precio
        carrito.save()
    
   
    return redirect(to="catalogoarte")


@login_required
def aumentarcantidad(request,id):
    item=itemCarrito.objects.get(id=id)
    carrito=cart.objects.get(user=request.user.username)

    item.cantidad+=1
    item.subtotal=item.precio*item.cantidad
    item.save()
    carrito.total+=item.precio
    carrito.save()

    return redirect(to="carrito")
@login_required
def restarcantidad(request,id):
    item=itemCarrito.objects.get(id=id)
    carrito=cart.objects.get(user=request.user.username)

    item.cantidad-=1
    item.subtotal=item.precio*item.cantidad
    item.save()
    carrito.total-=item.precio
    carrito.save()
    return redirect(to="carrito")

@login_required
def eliminaritem(request,id):
    item=itemCarrito.objects.get(id=id)
    carrito=cart.objects.get(user=request.user.username)
    carrito.total-=item.subtotal
    carrito.save()
    item.delete()
    return redirect(to="carrito")

@login_required
def generarpedido(request):
    carritoped=cart.objects.get(user=request.user.username)
    itemscarrito=itemCarrito.objects.all().filter(user=request.user.username)
    nuevopedido=Pedido()
    
    nuevopedido.user=request.user.username
    nuevopedido.monto=carritoped.total
    nuevopedido.save()
    for item in itemscarrito:
        print(item.producto)
        itemspedido=ItemPedido()
        itemspedido.pedido=nuevopedido.id
        itemspedido.producto=item.producto
        itemspedido.precio= item.precio
        itemspedido.cantidad=item.cantidad
        itemspedido.subtotal=item.subtotal
        itemspedido.save()
        item.delete()

    carritoped.total=0
    carritoped.save()
    

    return redirect(to="homecliente")

@login_required
def listadopedidos(request):
    pedidos=Pedido.objects.all().filter(user=request.user.username)

    contexto={
        'pedidos':pedidos
    }
    return render(request,'sitio_cliente/listadopedidos.html',contexto)

@login_required
def detallepedido(request,id):
    pedidodet=Pedido.objects.get(id=id)
    items=ItemPedido.objects.all().filter(pedido=id)

    contexto={
        'items':items,
        'pedidodet':pedidodet

    }

    return render(request,'sitio_cliente/detallepedido.html',contexto)
#Api Productos

from rest_framework import viewsets
from .serializers import srzJuego


class Juegosrz(viewsets.ModelViewSet):
    queryset= Juego.objects.all()
    serializer_class=srzJuego