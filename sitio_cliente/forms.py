from django import forms
from django.db.models import fields
from django.forms import widgets
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class formreviewjuego(forms.ModelForm):
    review=forms.Textarea(attrs={'class':'reviewuser'} )
    puntaje=forms.Select(attrs={'class':'selectpicker'})
    class Meta:
        model=reviewjuego
        fields=['review','recomienda']

class formreviewarte(forms.ModelForm):
     

     class Meta:
         model=reviewArte
         fields=['review','recomienda']


class FrmRegistro(UserCreationForm):
    class Meta:
        model=User
        fields=["username","email"]

class frmLog(forms.ModelForm):
    
    class Meta:
        model=User
        fields=["username","password"]

