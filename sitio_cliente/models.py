from django.db import models
from django.db.models.deletion import PROTECT
from django.core.validators import MinValueValidator, MaxValueValidator
from sitio_admin.models import *
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

puntaje=[('1','Pesimo'),
        ('2','Malo'),
        ('3','Regular'),
        ('4','Bueno'),
        ('5','Excelente'),
        ]

recomienda=[
    ('1','No lo Recomienda'),
    ('2','Lo Recomienda')
]  
tipo=[
    ('1','Juego'),
    ('2','Arte')
]  

estado=[
    ('1','Esperando deposito'),
    ('2','En camino'),
    ('3','Cerrado'),
    ('4','Cancelado'),
]
class reviewjuego(models.Model):
    idreview=models.AutoField(primary_key=True)
    juego=models.ForeignKey(Juego, on_delete=CASCADE, null=True)
    user=models.CharField(max_length=150)
    review=models.TextField(max_length=800,null=False)
    recomienda=models.CharField(max_length=1,choices=recomienda,default=2)


class reviewArte(models.Model):
    idreview=models.AutoField(primary_key=True)
    arte=models.ForeignKey(Arte,on_delete=CASCADE)
    user=models.CharField(max_length=150)
    review=models.TextField(max_length=800,null=False)
    recomienda=models.CharField(max_length=1,choices=recomienda,default=2)



class cart(models.Model):
    user=models.CharField(max_length=150,primary_key=True)    
    total=models.FloatField()
    def __str__(self):
        return self.user

class itemCarrito(models.Model):
    id=models.AutoField(primary_key=True)
    user=models.CharField(max_length=150)
    producto=models.CharField(max_length=250)
    precio=models.IntegerField()
    cantidad=models.IntegerField()
    subtotal=models.FloatField()
    

    

class Pedido(models.Model):
    id=models.AutoField(primary_key=True)
    user=models.CharField(max_length=150)
    fecha=models.DateTimeField(default=timezone.now)
    monto=models.IntegerField()
    estado=models.CharField(max_length=1,choices=estado,default='1')

class ItemPedido(models.Model):
    id=models.AutoField(primary_key=True)
    pedido=models.CharField(max_length=10)
    producto=models.CharField(max_length=250)
    precio=models.IntegerField()
    cantidad=models.IntegerField()
    subtotal=models.FloatField()


## class itemCarrito(models.Model):
#     id=models.AutoField(primary_key=True)
#     user=models.CharField(max_length=150)
#     producto=models.CharField(max_length=20)
#     es_juego=models.BooleanField(default=True)
        