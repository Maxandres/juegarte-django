from django.contrib import admin
from .models import   ItemPedido, Pedido, cart, itemCarrito,  reviewjuego

# Register your models here.
class AdmReviewjuego(admin.ModelAdmin):
    list_display=['idreview','juego','review','recomienda','user']
    class Meta:
        model=reviewjuego

admin.site.register(reviewjuego,AdmReviewjuego)        




class admItemCarrito(admin.ModelAdmin):
    list_display=['id','user','producto','precio','cantidad','subtotal']

  
admin.site.register(itemCarrito,admItemCarrito)   




class admcart(admin.ModelAdmin):
    list_display=['user','total']

admin.site.register(cart,admcart)


class admPedido(admin.ModelAdmin):
    list_display=['user','fecha','monto','estado']
admin.site.register(Pedido,admPedido)

class admItemPedido(admin.ModelAdmin):
    list_display=['pedido','producto','precio','cantidad','subtotal']

admin.site.register(ItemPedido,admItemPedido)    