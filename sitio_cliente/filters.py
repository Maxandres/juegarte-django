import django_filters
from django_filters import CharFilter
from sitio_admin.models import *

class filtroJuego(django_filters.FilterSet):
    nombrejuego= CharFilter(field_name='nombrejuego', lookup_expr='icontains',label="Nombre")
    class Meta:
        model=Juego
        fields=['nombrejuego','categoria']

class filtroArte(django_filters.FilterSet):
    nombrearte= CharFilter(field_name='nombrearte', lookup_expr='icontains',label="Nombre")
    class Meta:
        model=Arte
        fields=['nombrearte','categoria']        