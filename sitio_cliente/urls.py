from django.urls.conf import include
from sitio_cliente.views import home
from django.urls import path
from .views import Juegosrz, agregarartecarrito, agregarjuegocarrito, aumentarcantidad, carrito, catalogoarte, catalogojuegos, customLogin, detallearte, detallejuego, detallepedido, eliminaritem, generarpedido, home, listadopedidos, registro, restarcantidad
from rest_framework import routers

#para api
router=routers.DefaultRouter()
router.register('juegos',Juegosrz) 

urlpatterns = [
   path('',home,name="homecliente"),
   path('catalogojuegos/',catalogojuegos,name="catalogojuegos"),
   path('detallejuego/<id>',detallejuego,name="detallejuego"),
   path('catalogoarte/',catalogoarte,name="catalogoarte"),
   path('detallearte/<id>',detallearte,name="detallearte"),
   path('registro/',registro,name="registro"),
   path('carrito/',carrito,name="carrito"),
   path('catalogojuegos/agregarjuegocarrito/<id>',agregarjuegocarrito,name="agregarjuegocarrito"),
   path('catalogoarte/agregarartecarrito/<id>',agregarartecarrito,name="agregarartecarrito"),
   path('carrito/aumentarcantidad/<id>',aumentarcantidad,name="aumentarcantidad"),
   path('carrito/restarcantidad/<id>',restarcantidad,name="restarcantidad"),
   path('carrito/eliminaritem/<id>',eliminaritem,name="eliminaritem"),
   path('carrito/generarpedido',generarpedido,name="generarpedido"),
   path('pedidos/',listadopedidos,name="listadopedidos"),
   path('pedidos/detallepedido/<id>',detallepedido,name="detallepedido"),
   path('api/',include(router.urls)),
   
]