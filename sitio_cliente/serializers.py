from django.db.models import fields
from sitio_admin.models import Juego,Arte
from rest_framework import serializers

class srzJuego(serializers.ModelSerializer):
    class Meta:
        model=Juego
        fields=["idjuego","nombrejuego","preciojuego","descripcion","categoria","estado","imagen"]