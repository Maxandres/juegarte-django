
from django.contrib import admin
from .models import Arte, Categoriajuego,Categoriaarte, Juego

# Register your models here.

class AdmCategoriajuego(admin.ModelAdmin):
    list_display=["idcategoria","nombrecategoria"]
    

    class Meta:
        model=Categoriajuego
admin.site.register(Categoriajuego,AdmCategoriajuego)

class AdmCategoriaarte(admin.ModelAdmin):
    list_display=["idcategoria","nombrecategoria"]
    

    class Meta:
        model=Categoriaarte
admin.site.register(Categoriaarte,AdmCategoriaarte)

class AdmJuego(admin.ModelAdmin):
    list_display=["idjuego","nombrejuego","preciojuego","descripcion"]
    list_filter=["categoria"]

    class Meta:
        model=Juego
        


admin.site.register(Juego,AdmJuego)

class AdmArte(admin.ModelAdmin):
    list_display=["idarte","nombrearte","precioarte","descripcion","dimensiones"]
    list_filter=["categoria"]

    class Meta:
        model=Arte

admin.site.register(Arte,AdmArte)