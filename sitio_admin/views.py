
from django.contrib import messages
from django.contrib.auth.models import User
from .forms import formArte, formJuego
from django.shortcuts import get_object_or_404, redirect, render
from .models import Juego,Arte
from sitio_cliente.models import Pedido, ItemPedido
from django.contrib.auth.decorators import login_required,permission_required
# Create your views here.
@login_required
@permission_required('sito_admin.can_add_arte')
def home(request):
    juegos=Juego.objects.all().order_by("idjuego")
    arte=Arte.objects.all().order_by("idarte")
    contexto={
        "juegos":juegos,
        "arte":arte
    }
    
    return render(request,"sitio_admin/home.html",contexto)

@login_required
@permission_required('sito_admin.can_add_arte')
def agregarjuego(request):
    formulario=formJuego(request.POST or None)
    contexto={
        "form":formulario
    }
    if request.method=="POST":
        formulario=formJuego(data=request.POST,files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="home")
    return render(request, "sitio_admin/agregarjuego.html",contexto)    

@login_required
@permission_required('sito_admin.can_add_arte')
def agregararte(request):
    formulario=formArte(request.POST or None)
    contexto={
        "form":formulario
    }
    if request.method=="POST":
        formulario=formArte(data=request.POST,files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"Arte Agregado")
            return redirect(to="home")
    return render(request, "sitio_admin/agregararte.html",contexto)


@login_required
@permission_required('sito_admin.can_add_arte')
def editarjuego(request,id):
    juego=get_object_or_404(Juego,idjuego=id)
    formulario=formJuego(instance=juego)
    
    contexto={
        "form":formulario
    }    


    if request.method=="POST":
        formulario=formJuego(data=request.POST,instance=juego,files=request.FILES)

        if formulario.is_valid():
            jmod=Juego.objects.get(idjuego=juego.idjuego)
            datos=formulario.cleaned_data

            jmod.idjuego=juego.idjuego
            jmod.nombrejuego=datos.get("nombrejuego")
            jmod.preciojuego=datos.get("preciojuego")
            jmod.descripcion=datos.get("descripcion")
            jmod.categoria=datos.get("categoria")
            jmod.estado=datos.get("estado")
            jmod.imagen=datos.get("imagen")
            jmod.save()
            
            

            
                
            messages.success(request,"Juego Modificiado")

            return redirect(to="home")

        contexto={
            "form":formulario
        }
    return render(request,"sitio_admin/editarjuego.html",contexto)    


@login_required
@permission_required('sito_admin.can_add_arte')
def eliminarjuego(request,id):
    juego=get_object_or_404(Juego,idjuego=id)
    juego.delete()

    return redirect(to="home")


@login_required
@permission_required('sito_admin.can_add_arte')
def editararte(request,id):
    arte=get_object_or_404(Arte,idarte=id)
    formulario=formArte(instance=arte)

    contexto={
        "form":formulario
    }  

    if request.method=="POST":
        formulario=formArte(data=request.POST,instance=arte,files=request.FILES)

        if formulario.is_valid():
            amod=Arte.objects.get(idarte=arte.idarte)
            datos=formulario.cleaned_data

            amod.idarte=arte.idarte
            amod.nombrearte=datos.get("nombrearte")
            amod.precioarte=datos.get("precioarte")
            amod.descripcion=datos.get("descripcion")
            amod.dimensiones=datos.get("dimensiones")
            amod.categoria=datos.get("categoria")
            amod.estado=datos.get("estado")
            amod.imagen=datos.get("imagen")
            amod.save()

            messages.success(request,"Arte Modificiado")
            return redirect(to="home")

        contexto={
            "form":formulario
        }
    return render(request,"sitio_admin/editararte.html",contexto)


@login_required
@permission_required('sito_admin.can_add_arte')
def eliminararte(request,id):
    arte=get_object_or_404(Arte,idarte=id)
    arte.delete()

    return redirect(to="home")


@login_required
@permission_required('sito_admin.can_add_arte')
def listaclientes(request):
    usuarios=User.objects.all().filter(is_staff=False)
    contexto={
        'usuarios':usuarios

    }
    return render(request,"sitio_admin/listaclientes.html",contexto)

@login_required
@permission_required('sito_admin.can_add_arte')
def listaempleados(request):
    usuarios=User.objects.all().filter(is_staff=True)
    contexto={
        'usuarios':usuarios

    }
    return render(request,"sitio_admin/listaempleados.html",contexto)    


@login_required
@permission_required('sito_admin.can_add_arte')
def listapedidos(request):
    pedidos=Pedido.objects.all()
    print(request.user.email)
    print(request.user.username)
    contexto={
        'pedidos':pedidos
    }

    return render(request,'sitio_admin/listapedidos.html',contexto)

@login_required
@permission_required('sito_admin.can_add_arte')
def detallepedido(request,id):
    pedidodet=Pedido.objects.get(id=id)
    items=ItemPedido.objects.all().filter(pedido=id)

    contexto={
        'items':items,
        'pedidodet':pedidodet

    }

    return render(request,'sitio_admin/detallepedido.html',contexto)

@login_required
@permission_required('sito_admin.can_add_arte')
def cambiarestado(request,id,tipo):
    pedidodet=Pedido.objects.get(id=id)

    pedidodet.estado=tipo
    pedidodet.save()
    return redirect(to="listapedidos")
