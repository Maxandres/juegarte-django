from django import forms
from django.db.models import fields
from .models import Arte, Juego

class formJuego(forms.ModelForm):
    
    categoria=forms.SelectMultiple(attrs={'class':'selectpicker'})
     
    class Meta:
        model=Juego
        fields=["nombrejuego","preciojuego","descripcion","categoria","estado","imagen"]

  

class formArte(forms.ModelForm):


    class Meta:
        model=Arte
        fields=["nombrearte","precioarte","descripcion","dimensiones","categoria","estado","imagen"]