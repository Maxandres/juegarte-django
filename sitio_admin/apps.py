from django.apps import AppConfig


class SitioAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sitio_admin'
