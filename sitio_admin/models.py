from django.db import models
from django.db.models.deletion import CASCADE, PROTECT

# Create your models here.
Estadoproducto=[
    ("1","Disponible"),
    ("2","Oculto")
]
class Categoriajuego(models.Model):
    idcategoria=models.AutoField(primary_key=True)
    nombrecategoria=models.CharField(max_length=50,null=False)
   

    def __str__(self):
        return self.nombrecategoria

class Categoriaarte(models.Model):
    idcategoria=models.AutoField(primary_key=True)
    nombrecategoria=models.CharField(max_length=50,null=False)
   

    def __str__(self):
        return self.nombrecategoria


class Juego(models.Model):
    idjuego=models.AutoField(primary_key=True)
    nombrejuego=models.CharField(max_length=300,null=False)
    preciojuego=models.IntegerField(null=False)
    descripcion=models.TextField(max_length=800,null=False)
    categoria=models.ForeignKey(Categoriajuego,on_delete=CASCADE ,default=1)
    estado=models.CharField(max_length=1,choices=Estadoproducto,default="1")
    imagen=models.ImageField(upload_to="imagenes",null=True)

    def __str__(self):
        return self.nombrejuego
    

class Arte(models.Model):
    idarte=models.AutoField(primary_key=True)
    nombrearte=models.CharField(max_length=150,null=False)
    precioarte=models.IntegerField(null=False)
    descripcion=models.TextField(max_length=500,null=False)
    dimensiones=models.CharField(max_length=100)
    categoria=models.ForeignKey(Categoriaarte, on_delete=CASCADE, default=1)
    estado=models.CharField(max_length=1,choices=Estadoproducto,default="1")
    imagen=models.ImageField(upload_to="imagenes",null=True)

    def __str__(self):
        return self.nombrearte




