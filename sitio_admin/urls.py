
from django.urls import path
from .views import agregararte, agregarjuego, cambiarestado, detallepedido, editararte, editarjuego, eliminararte, eliminarjuego, home, listaclientes, listaempleados, listapedidos
urlpatterns = [
    path('', home, name="home" ),
    path('agregarjuego/', agregarjuego, name="agregarjuego" ),
    path('agregararte/', agregararte, name="agregararte" ),
    path('editarjuego/<id>', editarjuego, name="editarjuego" ),
    path('editararte/<id>',editararte,name="editararte"),
    path('eliminarjuego/<id>',eliminarjuego, name="eliminarjuego" ),
    path('eliminararte/<id>',eliminararte,name="eliminararte"),
    path('listaclientes/',listaclientes,name="listaclientes"),
    path('listaempleados/',listaempleados,name="listaempleados"),
    path('listapedidos/',listapedidos,name="listapedidos"),
    path('listapedidos/detallepedido/<id>',detallepedido,name="detallepedidoadm"),
    path('listapedidos/cambiarestado/<id>/<tipo>',cambiarestado,name="cambiarestado"),
    
]